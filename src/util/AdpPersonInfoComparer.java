package util;

import entities.AdpPersonInfo;

import java.util.Comparator;

/**
 * Created by Rodolfo Baeza
 * Date: 9/5/13
 * Time: 11:41 AM
 * Summary:
 */
public class AdpPersonInfoComparer implements Comparator<AdpPersonInfo> {
    @Override
    public int compare(AdpPersonInfo record1, AdpPersonInfo record2) {

        int res = record1.getLastTotalizationDateTime().compareTo(record2.getLastTotalizationDateTime());
        if(res > 0)
            return 1;

        if(res < 0)
            return -1;
        else
            return 0;
    }
}
