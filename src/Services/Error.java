package Services;
public enum Error {

	  CONNECTION(101, "A connection error has ocurred.");

	  private final int code;
	  private final String description;
	  

	  private Error(int code, String description) {
	    this.code = code;
	    this.description = description;
	  }

	  public String getDescription() {
	     return description;
	  }

	  public int getCode() {
	     return code;
	  }
	  

	  @Override
	  public String toString() {
	    return code + ": " + description;
	  }
	}