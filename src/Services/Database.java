package Services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Database {

    public static final int ENV_TEST = 1;
    public static final int ENV_PROD = 2;

    private static Database DatabaseObject;

    private Database() {
    }

    public static Database getDatabaseObject() {
        if (DatabaseObject == null)
            DatabaseObject = new Database();
        return DatabaseObject;
    }

    public Connection getConnection(int env) throws SQLException {

        Connection conn = null;

        String user;
        String pass;
        String url;

        switch (env) {
            case ENV_TEST:
                user = "xxetime";
                pass = "xxetime";
                url = "jdbc:oracle:thin:@devldb.savi.local:1591:TEST";
                break;

            case ENV_PROD:
                user = "xxetime";
                pass = "et4dmin";
                url = "jdbc:oracle:thin:@proddb.savi.local:1526:PROD";
                break;

            default:
                throw new IllegalArgumentException("Wrong ENV set for database connection.");
        }

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            conn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;


    }

}

