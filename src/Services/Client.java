package Services;

import entities.AdpPersonInfo;
import entities.PeriodTotalData;
import entities.Person;
import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import util.AdpPersonInfoComparer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;


public final class Client{

    private final Logger logger = Logger.getLogger(Client.class);

    private static final int CURRENT_ENV = Database.ENV_PROD;

    private static final String XML_SERVICE_URL = "http://br-time.savi.local/1/XmlService";
    private static Client ClientSingleton;
    private final DocumentBuilder documentBuilder;

    private Client() throws ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

        dbFactory.setNamespaceAware(false);
        dbFactory.setValidating(false);
        documentBuilder = dbFactory.newDocumentBuilder();
    }
	private static String cookie;

    public static Client getClientObject() throws ParserConfigurationException {
      if (ClientSingleton == null)
    	  ClientSingleton = new Client();
      return ClientSingleton;
    }
    
   	public void startSession() throws IOException
	{
   		logger.info("Entering Client.startSession");
		URL url = new URL(XML_SERVICE_URL);

        try{
		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-type","text/xml");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setUseCaches(false);
		
		
		String logon_request = "<Request Action=\"Logon\" Object=\"System\" Username=\"SuperUser\" Password=\"adP3T1m3!\" />";
		
		String xml_send = "<?xml version=\"1.0\"?>" +
							"<Kronos_WFC version=\"1.0\">" +
								logon_request +
							"</Kronos_WFC>";
		PrintWriter out = new PrintWriter(new OutputStreamWriter( conn.getOutputStream() ));

        out.println(xml_send);
        out.close();
		conn.connect();


		
		InputStream is = conn.getInputStream();
		int size = conn.getContentLength();
		byte[] dataBytes = new byte[size];
		int bytesRead = 0;
		int pos = 0;
		
		//read stream into byte array
		while ( (bytesRead >= 0) && (pos < size)) {
		if ((bytesRead = is.read(dataBytes, pos,
			size-pos)) >= 0) {
			pos = pos + bytesRead;
			}
		}

            is.close();
		
		//Store the cookie
		Vector<String> cookies = new Vector<String>();
		int j = 1;//

		while(true){
			String key = conn.getHeaderFieldKey(j);
			if (key==null)
			break;
				else if (key.equals("Set-Cookie")) {
					String value = conn.getHeaderField(j);
					cookies.addElement(value);
				}
			j++;
			}
			
			
			Client.cookie = cookies.toString();
			
			
		}
		catch(UnknownHostException e){
            e.printStackTrace();
			logger.error("Connection error: " + e.getMessage());
			System.out.println("Connection error @" + e.getMessage());
			
		}
        logger.info("Exiting Client.startSession");
    }

   	final String callWSMethod(String request) throws IOException
	{
		URL url = new URL(XML_SERVICE_URL);
		String response;
		try{
			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-type","text/xml");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(true);
			conn.setRequestProperty("cookie", cookie);
			
			String xml_send = "<?xml version=\"1.0\"?>" +
								"<Kronos_WFC version=\"1.0\">" +
									request +
								"</Kronos_WFC>";
			PrintWriter out = new PrintWriter(new OutputStreamWriter( conn.getOutputStream() ));
	
			//System.out.println(xml_send);
	        out.println(xml_send);
	        out.close();
			conn.connect();
			
			InputStream is = conn.getInputStream();
			int size = conn.getContentLength();
			byte[] dataBytes = new byte[size];
			int bytesRead = 0;
			int pos = 0;
			
			//read stream into byte array
			while ( (bytesRead >= 0) && (pos < size)) {
			if ((bytesRead = is.read(dataBytes, pos,
				size-pos)) >= 0) {
				pos = pos + bytesRead;
				}
			}
			
			response = new String(dataBytes, 0, size);
			is.close();

			return response;
		}
		catch(UnknownHostException e){
			logger.error("Connection error: " + e.getMessage());
			System.out.println("Connection error @" + e.getMessage());
			
		}
		return null;
	}

    /*
	public String generateXMLFromTable() throws SQLException, TransformerFactoryConfigurationError, ParserConfigurationException, TransformerException, SAXException
	{
		
		String response = null;
		try{
			URL url = new URL(XML_SERVICE_URL);
			
			String result = null;
			Database db = Database.getDatabaseObject();
			
			Connection con = db.getConnection(CURRENT_ENV);
			
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery("SELECT * from WIP_JOB_ST where rownum <= 10");
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder = factory.newDocumentBuilder();
		    DOMImplementation impl = builder.getDOMImplementation();
		    Document doc = impl.createDocument(null, null, null);
		    Element e1 = doc.createElement("Request");
		    doc.appendChild(e1);
		    e1.setAttribute("Object", "LaborLevelEntry");
		    e1.setAttribute("Action","AddOnly");
		    
		    
			//String response = ws.callWSMethod("<Request Action=\"AddOnly\" Object=\"LaborLevelEntry\" LaborLevelEntryName=\"New Test Job\" 
		    //LaborLevelDefinitionName=\"Job\"></Request>");

			while(rs.next()){
				Element tag = doc.createElement("LaborLevelEntry");
				e1.appendChild(tag);
				tag.setAttribute("LaborLevelEntryName", rs.getString("WIP_ENTITY_NAME"));
				//tag.setAttribute("Description", rs.getString("DESCRIPTION"));
				tag.setAttribute("Description", rs.getString("WIP_ENTITY_NAME"));
				tag.setAttribute("InactiveFlag","false");
				tag.setAttribute("LaborLevelDefinitionName","Job");
			}
			
		    DOMSource domSource = new DOMSource(doc);
		    
		    Transformer transformer = getTransformerObject();
		    
		    StringWriter sw = new StringWriter();
		    StreamResult sr = new StreamResult(sw);
		    transformer.transform(domSource, sr);
		    
		    String string_xml = sw.toString();
		    //System.out.println(string_xml);

			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-type","text/xml");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setRequestProperty("cookie", cookie);
			
			String xml_send = "<?xml version=\"1.0\"?>" +
								"<Kronos_WFC version=\"1.0\">" +
									string_xml +
								"</Kronos_WFC>";
			PrintWriter out = new PrintWriter(new OutputStreamWriter( conn.getOutputStream() ));
	
	        out.println(xml_send);
	        out.close();
			conn.connect();
			
			InputStream is = conn.getInputStream();
			int size = conn.getContentLength();
			byte[] dataBytes = new byte[size];
			int bytesRead = 0;
			int pos = 0;
			
			//read stream into byte array
			while ( (bytesRead >= 0) && (pos < size)) {
			if ((bytesRead = is.read(dataBytes, pos,
				size-pos)) >= 0) {
				pos = pos + bytesRead;
				}
			}
			
			response = new String(dataBytes, 0, size);
			is.close();

			return response;
		}
		catch(UnknownHostException e){
			
			System.out.println("Connection error @" + e.getMessage());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}
	*/

	public String uploadJobs() throws SQLException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException {
        logger.info("Entering Client.uploadJobs");
		String response = null;
		try{
			URL url = new URL(XML_SERVICE_URL);

            Database db = Database.getDatabaseObject();
			
			Connection con = db.getConnection(CURRENT_ENV);
			
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery("SELECT wip_entity_name, description from WIP_JOB_ST where LOADED_INTO_ADP='N' group by wip_entity_name, description");
            //ResultSet rs = s.executeQuery("SELECT wip_entity_name, description from WIP_JOB_ST where wip_entity_name='1982248' group by wip_entity_name, description");
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder = factory.newDocumentBuilder();
		    DOMImplementation impl = builder.getDOMImplementation();
		    Document doc = impl.createDocument(null, null, null);
		    Element e1 = doc.createElement("Request");
		    doc.appendChild(e1);
		    e1.setAttribute("Object", "LaborLevelEntry");
		    e1.setAttribute("Action","AddOnly");

			//String response = ws.callWSMethod("<Request Action=\"AddOnly\" Object=\"LaborLevelEntry\" LaborLevelEntryName=\"New Test Job\" 
		    //LaborLevelDefinitionName=\"Job\"></Request>");

            con.setAutoCommit(false);
            int jobsLoaded = 0;

			while(rs.next()){

                String description = rs.getString("DESCRIPTION");
                if(description != null) {
                    description = description.replace(':', '.');
                    description = description.replace('(', ' ');
                    description = description.replace(')', ' ');
                }
                String name = rs.getString("WIP_ENTITY_NAME");

				Element tag = doc.createElement("LaborLevelEntry");
				e1.appendChild(tag);
				tag.setAttribute("LaborLevelEntryName", name);
				//tag.setAttribute("Description", rs.getString("DESCRIPTION"));
				tag.setAttribute("Description", description);
				tag.setAttribute("InactiveFlag","false");
				tag.setAttribute("LaborLevelDefinitionName","Job");

                PreparedStatement updStatement = con.prepareStatement("update WIP_JOB_ST set LOADED_INTO_ADP='Y' where WIP_ENTITY_NAME=?");
                updStatement.setString(1, name);
                updStatement.execute();
                updStatement.close();
                logger.info("Job imported: " + name);
                jobsLoaded++;
			}

            con.commit();
            con.setAutoCommit(true);
			
		    DOMSource domSource = new DOMSource(doc);
		    
		    Transformer transformer = getTransformerObject();
		    
		    StringWriter sw = new StringWriter();
		    StreamResult sr = new StreamResult(sw);
		    transformer.transform(domSource, sr);
		    
		    String string_xml = sw.toString();
		    //System.out.println(string_xml);

			HttpURLConnection conn = (HttpURLConnection)url.openConnection();
			
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-type","text/xml");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			conn.setRequestProperty("cookie", cookie);
			
			String xml_send = "<?xml version=\"1.0\"?>" +
								"<Kronos_WFC version=\"1.0\">" +
									string_xml +
								"</Kronos_WFC>";
			PrintWriter out = new PrintWriter(new OutputStreamWriter( conn.getOutputStream() ));
	
	        out.println(xml_send);
	        out.close();
			conn.connect();
			
			InputStream is = conn.getInputStream();
			int size = conn.getContentLength();
			byte[] dataBytes = new byte[size];
			int bytesRead = 0;
			int pos = 0;
			
			//read stream into byte array
			while ( (bytesRead >= 0) && (pos < size)) {
			if ((bytesRead = is.read(dataBytes, pos,
				size-pos)) >= 0) {
				pos = pos + bytesRead;
				}
			}
			
			response = new String(dataBytes, 0, size);
            //System.out.println(response);
			is.close();

            logger.info("Exiting Client.uploadJobs. Jobs loaded: " + jobsLoaded);
			return response;
		}
		catch(UnknownHostException e){
			logger.error("Connection error: " + e.getMessage());
			System.out.println("Connection error @" + e.getMessage());
			
		} catch (IOException e) {
            logger.error("IOException error: " + e.getMessage());
			e.printStackTrace();
		}
        logger.info("Exiting Client.uploadJobs");
		return response;
	}

    /*
	public String updateEmployeesJob() throws SQLException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException, ParseException {
		String response = null;
		try{
			URL url = new URL(XML_SERVICE_URL);
			
			String result = null;
			Database db = Database.getDatabaseObject();
			
			Connection con = db.getConnection(CURRENT_ENV);
			
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery("SELECT * from WIP_JOB_ST");
		    
		    
			//String response = ws.callWSMethod("<Request Action=\"AddOnly\" Object=\"LaborLevelEntry\" LaborLevelEntryName=\"New Test Job\" 
		    //LaborLevelDefinitionName=\"Job\"></Request>");

			while(rs.next()){
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			    DocumentBuilder builder = factory.newDocumentBuilder();
			    DOMImplementation impl = builder.getDOMImplementation();
			    Document doc = impl.createDocument(null, null, null);
			    Element e1 = doc.createElement("Request");
			    doc.appendChild(e1);
			    e1.setAttribute("Object", "Personality");
			    e1.setAttribute("Action","UpdateOnly");

			    Element tagPersonality = doc.createElement("Personality");
				e1.appendChild(tagPersonality);
				Element tagIdentity = doc.createElement("Identity");
				tagPersonality.appendChild(tagIdentity);
				Element tagPersonIdentity = doc.createElement("PersonIdentity");
				tagIdentity.appendChild(tagPersonIdentity);
				tagPersonIdentity.setAttribute("BadgeNumber", rs.getString("EMPLOYEE_NUMBER"));
				
				Element tagJobAssignmentData = doc.createElement("JobAssignmentData");
				tagPersonality.appendChild(tagJobAssignmentData);
				Element tagJobAssignment = doc.createElement("JobAssignment");
				tagJobAssignmentData.appendChild(tagJobAssignment);
				Element tagPrimaryLaborAccounts = doc.createElement("PrimaryLaborAccounts");
				tagJobAssignment.appendChild(tagPrimaryLaborAccounts);
				Element tagPrimaryLaborAccount = doc.createElement("PrimaryLaborAccount");
				tagPrimaryLaborAccounts.appendChild(tagPrimaryLaborAccount);
				
				String startDateString = rs.getString("SCHEDULED_START_DATE");
				Date startDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(startDateString);
				String newStartDateString = new SimpleDateFormat("MM/dd/yyyy").format(startDate);
				tagPrimaryLaborAccount.setAttribute("EffectiveDate", newStartDateString);
				
				String endDateString = rs.getString("SCHEDULED_COMPLETION_DATE");
				Date endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDateString);
				String newEndDateString = new SimpleDateFormat("MM/dd/yyyy").format(endDate);
				tagPrimaryLaborAccount.setAttribute("ExpirationDate", newEndDateString);
				
				StringBuilder sb = new StringBuilder();
				sb.append(rs.getString("ORG_ID"));
				sb.append("/");
				sb.append(rs.getString("COST_CENTER"));
				sb.append("/");
				sb.append(rs.getString("PRODUCT"));
				sb.append("/");
				sb.append(rs.getString("ACCOUNT_NO"));
				sb.append("/");
				sb.append(rs.getString("SUPERVISOR"));
				sb.append("/");
				sb.append(rs.getString("DEPARTMENT"));
				sb.append("/");
				sb.append(rs.getString("JOB_NAME"));
				
				tagPrimaryLaborAccount.setAttribute("LaborAccountName", sb.toString());

			    DOMSource domSource = new DOMSource(doc);
			    
			    Transformer transformer = getTransformerObject();
			    
			    StringWriter sw = new StringWriter();
			    StreamResult sr = new StreamResult(sw);
			    transformer.transform(domSource, sr);
			    
			    String string_xml = sw.toString();
			    System.out.println("******** XML TO SEND *********");
			    System.out.println(string_xml);

				HttpURLConnection conn = (HttpURLConnection)url.openConnection();
				
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-type","text/xml");
				conn.setDoOutput(true);
				conn.setDoInput(true);
				conn.setUseCaches(false);
				conn.setRequestProperty("cookie", cookie);
				
				String xml_send = "<?xml version=\"1.0\"?>" +
									"<Kronos_WFC version=\"1.0\">" +
										string_xml +
									"</Kronos_WFC>";
				PrintWriter out = new PrintWriter(new OutputStreamWriter( conn.getOutputStream() ));
		
		        out.println(xml_send);
		        
		        out.close();
				conn.connect();
				
				InputStream is = conn.getInputStream();
				int size = conn.getContentLength();
				byte[] dataBytes = new byte[size];
				int bytesRead = 0;
				int pos = 0;
				
				//read stream into byte array
				while ( (bytesRead >= 0) && (pos < size)) {
				if ((bytesRead = is.read(dataBytes, pos,
					size-pos)) >= 0) {
					pos = pos + bytesRead;
					}
				}
				
				response = new String(dataBytes, 0, size);
				is.close();
			    System.out.println("******** RESPONSE *********");
				System.out.println(response);
			}
			
			return response;
		}
		catch(UnknownHostException e){
			
			System.out.println("Connection error @" + e.getMessage());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;

	}
	*/

	public void updateOracleData(String dateSpan) throws IOException, SAXException {
        logger.info("Entering Client.updateOracleData");
		//String response = null;
        Connection con = null;
        long startTime = System.nanoTime();
        long endTime;
        int employeesProcessed = 0;

        try {
	        String errorDetails = "";
            Database db = Database.getDatabaseObject();

            con = db.getConnection(CURRENT_ENV);
            con.setAutoCommit(false);

            String insertQuery = "insert into ADP_INFO(BADGE_NUMBER,PAYRULENAME,BASEWAGEHOURLY,EFFECTIVEDATE,EXPIRATIONDATE,DEF_LABORACCOUNTNAME," +
                    "HOURLYRATE,ADDRESS,FIRSTNAME,LASTNAME,PERSONNUMBER,FULLTIMEPERCENTAGE,LABORACCOUNTNAME,AMOUNTINCURRENCY,PAYCODENAME," +
                    "AMOUNTINTIME,LASTTOTALIZATIONDATETIME,RECORD_STATUS, RECORDDATE) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement st = con.prepareStatement(insertQuery);

            ArrayList<String> employees = getEmployeeNumbers(dateSpan);
            employeesProcessed = employees.size();
            for (String personNumber : employees) {
                try {

                    //String personNumber = "HIU0000127"; // Jason Lee (UserName=000127)
                    //String personNumber = "HIU0000061"; // Glenn Leblanc (UserName=000061)
                    //String personNumber = "HIU0010035"; // Ryan Kelly (UserName=0482)
                    //String personNumber = "HIU0005156"; // Joseph Banks (UserName=5156)

                    //String period = "Current Pay Period"; // Current Period
                    //String period = "Previous Pay Period"; // Previous Period

                    Person person = getPerson(personNumber);

                    ArrayList<PeriodTotalData> totals = getPeriodTotals(personNumber, dateSpan, person, con);

                    for (PeriodTotalData total : totals) {


	                    String[] elements = total.getLaborAccountName().split("/");
	                    if(elements.length > 0) {
		                    String jobName = elements[elements.length-1];

		                    if (jobName.equalsIgnoreCase("LUL2304690")) {
			                    System.out.println("*** INSERTING Job LUL2304690 found: " + total.toString());
		                    }
	                    }

	                    errorDetails = total.getLaborAccountName();

		                st.setString(1, person.getBadgeNumber());
                        st.setString(2, person.getPayRuleName());
                        st.setString(3, person.getBaseWageHourly());
                        st.setString(4, person.getEffectiveDate());
                        st.setString(5, person.getExpirationDate());
                        st.setString(6, person.getDefLaborAccountName());
                        st.setString(7, person.getHourlyRate());
                        st.setString(8, person.getAddress());
                        st.setString(9, person.getFirstName());
                        st.setString(10, person.getLastName());
                        st.setString(11, person.getPersonNumber());
                        st.setString(12, person.getFullTimePercentage());
                        st.setString(13, total.getLaborAccountName());
                        st.setString(14, total.getAmountInCurrency());
                        st.setString(15, total.getPayCodeName());
                        st.setString(16, total.getAmountInTime());
                        st.setString(17, total.getLastTotalizationDateTime());
                        st.setString(18, total.getRecordStatus());
                        st.setString(19, total.getRecordDate());

                        st.executeUpdate();
                        st.clearParameters();

                    }
                } catch (Exception e) {

	                logger.error("Exception: " + e.getMessage());
	                logger.info("Error details: " + errorDetails);
                    con.rollback();
                    e.printStackTrace();

                }

            }
            con.commit();
            con.setAutoCommit(true);
        } catch(SQLException sqle) {
            logger.error("SQLException: " + sqle.getMessage());
            sqle.printStackTrace();
        } finally {
            if(con != null)
                try {
                    con.close();
                } catch (SQLException e) {
                    logger.error("SQLException: " + e.getMessage());
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
        }

        endTime = System.nanoTime();
        long duration = (endTime - startTime) / 1000000000;

        logger.info("Exiting Client.updateOracleData. Time ellapsed: " + duration + " seconds. Employees processed: " + employeesProcessed);
	}

    private ArrayList<String> getEmployeeNumbers(String dateSpan) throws IOException, SAXException {
        logger.info("Entering Client.getEmployeeNumbers");

        /*
        ArrayList<String> numbers = new ArrayList<String>();
        numbers.add("HIU0009988");
        numbers.add("HIU0000061");
        numbers.add("HIU0010035");
        numbers.add("HIU0005156");
        return numbers;
        */

        String response = callWSMethod("<Request Action=\"RunQuery\" > <HyperFindQuery HyperFindQueryName=\"All Home\" QueryDateSpan=\"" + dateSpan + "\" VisibilityCode=\"Public\" /></Request>");
        //System.out.println(response);
        Document doc = getXMLDocument(response);

        ArrayList<String> list = new ArrayList<String>();

        NodeList nodes = doc.getElementsByTagName("HyperFindResult");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            list.add(element.getAttribute("PersonNumber"));
        }
        logger.info("Exiting Client.getEmployeeNumbers (" + list.size() + ")");
        return list;
    }

    public String getCurrentDaySpan() throws IOException, SAXException {
        logger.info("Entering Client.getCurrentSpan");
        //String response = callWSMethod("<Request Action=\"LoadDateRange\" > <TimeFramePeriod TimeFrameName=\"1\" PeriodDateSpan=\"08-01-2013 - 08-30-2013\" /></Request>");
        String response = callWSMethod("<Request Action=\"LoadDateRange\" > <TimeFramePeriod TimeFrameName=\"Previous schedule period\" /></Request>");

        //System.out.println(response);
        Document doc = getXMLDocument(response);

        String dateSpan = null;
        NodeList nodes = doc.getElementsByTagName("TimeFramePeriod");
        if(nodes != null && nodes.getLength() > 0) {
            Node node = nodes.item(0);
            Element element = (Element) node;
            dateSpan = element.getAttribute("PeriodDateSpan");
        }
        logger.info("Exiting Client.getCurrentSpan (" + dateSpan + ")");
        return dateSpan;
    }

    private Document getXMLDocument(String response) throws SAXException, IOException {
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(response));
        Document doc = documentBuilder.parse(is);
        doc.getDocumentElement().normalize();
        return doc;
    }

    private ArrayList<PeriodTotalData> getPeriodTotals(String personNumber, String period, Person person, Connection con) throws IOException, SAXException, SQLException {
        //String response = callWSMethod("<Request Action=\"LoadDailyTotals\" > <Timesheet> <Period> <TimeFramePeriod TimeFrameName=\""+ period + "\"/> </Period> <Employee> <PersonIdentity PersonNumber=\"" + personNumber + "\"/> </Employee> </Timesheet> </Request>");
        //period = "08/01/2013 - 09/01/2013";
        String response = callWSMethod("<Request Action=\"LoadDailyTotals\" > <Timesheet> <Period> <TimeFramePeriod PeriodDateSpan=\""+ period + "\"/> </Period> <Employee> <PersonIdentity PersonNumber=\"" + personNumber + "\"/> </Employee> </Timesheet> </Request>");
        //System.out.println(response);
        Document doc = getXMLDocument(response);

        ArrayList<PeriodTotalData> totals = new ArrayList<PeriodTotalData>();
        ArrayList<PeriodTotalData> totalsToInsert = new ArrayList<PeriodTotalData>();

        //Timesheet - LastTotalizationDateTime
        NodeList nodesTimesheet = doc.getElementsByTagName("Timesheet");
        String lastTotalizationDateTime = null;
        if(nodesTimesheet.getLength() == 1) {
            Node node = nodesTimesheet.item(0);
            Element element = (Element) node;
            lastTotalizationDateTime = element.getAttribute("LastTotalizationDateTime");
        }
        totals = getAllTotalsForEmployee(doc, totals, lastTotalizationDateTime);
        for(PeriodTotalData total : totals) {
            boolean insert = false;

            if (!VerifyRecordExist(person.getPersonNumber(), total, con)) {
                insert = true;

                //String text = String.format("NEW Employee: %s, total: %s", person.getPersonNumber(), total.toString());
                //System.out.println(text);
                //logger.info(text);

                PreparedStatement statement = null;
                ResultSet results = null;
                ArrayList<AdpPersonInfo> records = new ArrayList<AdpPersonInfo>();
                double amountInTime = 0.0;
                double amountInCurrency = 0.0;
                try {
                    String sql = "select personnumber, laboraccountname, paycodename, amountintime, amountincurrency, recorddate, lasttotalizationdatetime " +
                            "from ADP_INFO where personnumber = ? " +
                            "and recorddate = ? and paycodename = ? " +
                            "and laboraccountname = ? " +
                            "and record_status not in ('T', 'N')";
                    statement = con.prepareStatement(sql);
                    statement.setString(1, person.getPersonNumber());
                    statement.setString(2, total.getRecordDate());
                    statement.setString(3, total.getPayCodeName());
                    statement.setString(4, total.getLaborAccountName());
                    results = statement.executeQuery();
                    while(results.next()) {
                        AdpPersonInfo info = new AdpPersonInfo();
                        info.setNumber(results.getString("personnumber"));
                        info.setPayCodeName(results.getString("paycodename"));
                        info.setLaborAccountName(results.getString("laboraccountname"));
                        info.setAmountInTime(results.getString("amountintime"));
                        info.setRecordDate(results.getString("recorddate"));
                        info.setLastTotalizationDateTime(results.getString("lasttotalizationdatetime"));
                        records.add(info);
                        amountInTime += Double.parseDouble(info.getAmountInTime());
                        amountInCurrency += Double.parseDouble(results.getString("amountincurrency"));
                    }
                } catch (SQLException e) {
                    return null;
                } finally {
                    results.close();
                    statement.close();
                }

                if (amountInTime > 0) {

                    insert = false;

                    Double newestTime;
                    Double newestCurrency;
                    try {
                        newestTime = Double.parseDouble(total.getAmountInTime());
                        newestCurrency = Double.parseDouble(total.getAmountInCurrency());
                    } catch(NumberFormatException nfe) {
                        newestTime = 0.0;
                        newestCurrency = 0.0;
                    }
                    Double differenceInTime = newestTime - amountInTime;
                    Double differenceInCurrency = newestCurrency - amountInCurrency;
                    DecimalFormat df = new DecimalFormat("#.#####");

                    Double newTime = Double.valueOf(df.format(differenceInTime));

                    if (newTime > 0) {
                        insert = true;
                        total.setAmountInTime(df.format(differenceInTime));
                        total.setAmountInCurrency(df.format(differenceInCurrency));
                    }
                }
                else {
                    insert = true;
                }

            }

            if (insert) {
                totalsToInsert.add(total);
                //String text = String.format("INSERT Employee: %s, total: %s", person.getPersonNumber(), total.toString());
                //System.out.println(text);
                //logger.info(text);
            }
        }

        return totalsToInsert;
    }

    private ArrayList<PeriodTotalData> getAllTotalsForEmployee(Document doc, ArrayList<PeriodTotalData> totals, String lastTotalizationDateTime) throws SQLException {
        //Get all the Totals of the Timesheet
        NodeList nodes = doc.getElementsByTagName("DateTotals");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;

            String dateOfTotal = element.getAttribute("Date");

            NodeList nodesTotals = element.getElementsByTagName("Total");
            for(int n=0; n<nodesTotals.getLength(); n++) {
                Node nodeTotal = nodesTotals.item(n);
                Element elementTotal = (Element) nodeTotal;

                // Ignore the Total Hours elements
                String PAYCODE_TOTAL_OT = "201";
                String PAYCODE_TOTAL_HOURS = "202";
                if(elementTotal.getAttribute("PayCodeId").equals(PAYCODE_TOTAL_HOURS) ||
                        elementTotal.getAttribute("PayCodeId").equals(PAYCODE_TOTAL_OT))
                    continue;

                PeriodTotalData total = new PeriodTotalData();

                //Total - LaborAccountName, AmountInCurrency, PayCodeName, AmountInTime
                total.setLaborAccountName(elementTotal.getAttribute("LaborAccountName"));
                total.setAmountInCurrency(elementTotal.getAttribute("AmountInCurrency"));
                total.setPayCodeName(elementTotal.getAttribute("PayCodeName"));
                total.setAmountInTime(elementTotal.getAttribute("AmountInTime"));
                total.setRecordDate(dateOfTotal);
                total.setRecordStatus("P");
                total.setLastTotalizationDateTime(lastTotalizationDateTime);

                //System.out.println("JOB: " + total.toString());
                boolean insert = false;

                if (total.getLaborAccountName() != null) {
                    String[] elements = total.getLaborAccountName().split("/");
                    if(elements.length > 0) {
                        String jobName = elements[elements.length-1];

	                    if (jobName.equalsIgnoreCase("LUL2304690")) {
		                    System.out.println("Job LUL2304690 found: " + total.toString());
	                    }

                        if(jobName.equals("0") ||
                                jobName.equalsIgnoreCase("shop time") ||
                                jobName.equalsIgnoreCase("TRAINING"))
                           insert = false;
                        else {
                            if(total.getPayCodeName().equalsIgnoreCase("Regular")
                                    || total.getPayCodeName().equalsIgnoreCase("Overtime")
                                    || total.getPayCodeName().equalsIgnoreCase("OT")
                                    || total.getPayCodeName().equalsIgnoreCase("DT")) {
                                insert = true;
                                //System.out.println("JOB TO INSERT: " + total.toString());
                            }
                        }
                    }
                }

                if(insert) {

                    totals.add(total);

                }
            }
        }
        return totals;
    }

    private boolean VerifyRecordExist(String personNumber, PeriodTotalData total, Connection con) throws SQLException {
        PreparedStatement statement = null;
        ResultSet results = null;

        String sql = "select personnumber, laboraccountname, paycodename, amountintime, recorddate, lasttotalizationdatetime " +
                "from ADP_INFO where personnumber = ? and laboraccountname = ? and recorddate = ? " +
                " and amountintime = ? and paycodename = ? " +
                " and record_status in ('P', 'I')";
        statement = con.prepareStatement(sql);
        statement.setString(1, personNumber);
        statement.setString(2, total.getLaborAccountName());
        statement.setString(3, total.getRecordDate());
        statement.setString(4, total.getAmountInTime());
        statement.setString(5, total.getPayCodeName());
        results = statement.executeQuery();

	    String[] elements = total.getLaborAccountName().split("/");
	    if(elements.length > 0) {
		    String jobName = elements[elements.length-1];

		    if (jobName.equalsIgnoreCase("LUL2304690")) {
			    System.out.println("Job LUL2304690 found: " + total.toString());
		    }
	    }

		boolean exist = results.next();

        results.close();
        statement.close();

        return exist;
    }

    private Person getPerson(String personNumber) throws IOException, SAXException {
        String response = callWSMethod("<Request Action=\"Load\" Object=\"Personality\"> <Personality><Identity><PersonIdentity PersonNumber=\"" + personNumber + "\"/></Identity></Personality> </Request>");
        //String response = ws.callWSMethod("<Request Action=\"LoadPeriodTotals\" > <Timesheet> <Period> <TimeFramePeriod TimeFrameName=\"Current Pay Period\"/> </Period> <Employee> <PersonIdentity BadgeNumber=\"61\"/> </Employee> </Timesheet> </Request>");
        //String response = ws.callWSMethod("<Request Action=\"LoadPayCodeTotals\" Object=\"BulkTotals\" ><BulkTotals PayCodeNames=\"Regular,Overtime,Holiday\"><Employees><PersonIdentity BadgeNumber=\"61\"/></Employees><Period><TimeFramePeriod TimeFrameName=\"1\"/></Period></BulkTotals></Request>");
        //System.out.println(response);
        Document doc = getXMLDocument(response);

        Person person = new Person();

        //BadgeAssignment - BadgeNumber
        NodeList nodes = doc.getElementsByTagName("BadgeAssignment");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            person.setBadgeNumber(element.getAttribute("BadgeNumber"));
        }

        //JobAssignmentDetails - PayRuleName, BaseWageHourly
        nodes = doc.getElementsByTagName("JobAssignmentDetails");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            person.setPayRuleName(element.getAttribute("PayRuleName"));
            person.setBaseWageHourly(element.getAttribute("BaseWageHourly"));
        }
        //PrimaryLaborAccount - EffectiveDate, ExpirationDate, LaborAccountName
        nodes = doc.getElementsByTagName("PrimaryLaborAccount");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            person.setEffectiveDate(element.getAttribute("EffectiveDate"));
            person.setExpirationDate(element.getAttribute("ExpirationDate"));
            person.setDefLaborAccountName(element.getAttribute("LaborAccountName"));
        }
        //BaseWageRate - HourlyRate
        nodes = doc.getElementsByTagName("BaseWageRate");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            person.setHourlyRate(element.getAttribute("HourlyRate"));
        }
        //PostalAddress - PostalAddress + City + State + PostalCode
        nodes = doc.getElementsByTagName("PostalAddress");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            StringBuilder sb = new StringBuilder();
            sb.append(element.getAttribute("Street"));
            sb.append(", ");
            sb.append(element.getAttribute("City"));
            sb.append(", ");
            sb.append(element.getAttribute("State"));
            sb.append(", ");
            sb.append(element.getAttribute("PostalCode"));

            person.setAddress(sb.toString());
        }
        //Person - FirstName, LastName, PersonNumber, FullTimePercentage
        nodes = doc.getElementsByTagName("Person");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            person.setFirstName(element.getAttribute("FirstName"));
            person.setLastName(element.getAttribute("LastName"));
            person.setFullTimePercentage(element.getAttribute("FullTimePercentage"));
        }

        nodes = doc.getElementsByTagName("UserAccount");
        for(int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element element = (Element) node;
            person.setPersonNumber(element.getAttribute("UserName"));
        }

        return person;
    }

    /*
    private String performWSAction(URL url, ResultSet rs)
			throws ParserConfigurationException, SQLException,
			TransformerConfigurationException,
			TransformerFactoryConfigurationError, TransformerException,
			IOException, ProtocolException {
		
		String response;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		DOMImplementation impl = builder.getDOMImplementation();
		Document doc = impl.createDocument(null, null, null);
		Element e1 = doc.createElement("Request");
		doc.appendChild(e1);
		e1.setAttribute("Object", "Personality");
		e1.setAttribute("Action","Load");

		Element tagPersonality = doc.createElement("Personality");
		e1.appendChild(tagPersonality);
		Element tagIdentity = doc.createElement("Identity");
		tagPersonality.appendChild(tagIdentity);
		Element tagPersonIdentity = doc.createElement("PersonIdentity");
		tagIdentity.appendChild(tagPersonIdentity);
		tagPersonIdentity.setAttribute("BadgeNumber", rs.getString("BADGE_NUMBER"));
		
		DOMSource domSource = new DOMSource(doc);
		
		Transformer transformer = getTransformerObject();
		
		StringWriter sw = new StringWriter();
		StreamResult sr = new StreamResult(sw);
		transformer.transform(domSource, sr);
		
		String string_xml = sw.toString();
		System.out.println("******** XML TO SEND *********");
		System.out.println(string_xml);

		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-type","text/xml");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.setRequestProperty("cookie", cookie);
		
		String xml_send = "<?xml version=\"1.0\"?>" +
							"<Kronos_WFC version=\"1.0\">" +
								string_xml +
							"</Kronos_WFC>";
		PrintWriter out = new PrintWriter(new OutputStreamWriter( conn.getOutputStream() ));

		out.println(xml_send);
		
		out.close();
		conn.connect();
		
		InputStream is = conn.getInputStream();
		int size = conn.getContentLength();
		byte[] dataBytes = new byte[size];
		int bytesRead = 0;
		int pos = 0;
		
		//read stream into byte array
		while ( (bytesRead >= 0) && (pos < size)) {
		if ((bytesRead = is.read(dataBytes, pos,
			size-pos)) >= 0) {
			pos = pos + bytesRead;
			}
		}
		
		response = new String(dataBytes, 0, size);
		is.close();
		System.out.println("******** RESPONSE *********");
		System.out.println(response);
		return response;
	}
	*/

	private Transformer getTransformerObject()
			throws TransformerConfigurationException,
			TransformerFactoryConfigurationError {
		//transformer configuration.
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		return transformer;
	}

}