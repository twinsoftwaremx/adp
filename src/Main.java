import Services.Client;
import org.apache.log4j.Logger;
import org.boris.winrun4j.ServiceException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.boris.winrun4j.AbstractService;


public class Main extends AbstractService {

    private static Logger logger = Logger.getLogger(Main.class);

    private static String VERSION = "1.6.1";

/*
	public static void main(String[] args) throws SQLException, TransformerFactoryConfigurationError, ParserConfigurationException, TransformerException, IOException, SAXException, ParseException{
		logger.info("Starting the process...");
		Client ws = Client.getClientObject();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, cal.getActualMinimum(Calendar.DAY_OF_WEEK));
        cal.add(Calendar.WEEK_OF_YEAR, -1);
        Date firstDay = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

        cal.set(Calendar.DAY_OF_WEEK, cal.getActualMaximum(Calendar.DAY_OF_WEEK));
        cal.add(Calendar.WEEK_OF_YEAR, 1);
        Date lastDay = cal.getTime();

        //String dateSpan = String.format("%s - %s", sdf.format(firstDay), sdf.format(lastDay));
		//String firstDateString = args[0];
		//String secondDateString = args[1];
		//String dateSpan = String.format("%s - %s", firstDateString, secondDateString);
        //System.out.println(dateSpan);

        ws.startSession();
        //String dateSpan = ws.getCurrentDaySpan();
		String response = ws.uploadJobs();
        //String response = ws.callWSMethod("<Request Action=\"RunQuery\" > <HyperFindQuery HyperFindQueryName=\"All Home\" QueryDateSpan=\"05/20/2013 - 08/01/2013\" VisibilityCode=\"Public\" /></Request>");
        //String response = ws.callWSMethod("<Request Action=\"RunQuery\" > <HyperFindQuery HyperFindQueryName=\"All Home\" QueryDateSpan=\"Current Pay Period\" VisibilityCode=\"Public\" /></Request>");

        //System.out.println(dateSpan);
		//response = ws.updateEmployeesJob(dateSpan);
		//System.out.println(response);

		//ws.updateOracleData(dateSpan);
		
		//System.out.println(ws.cookie);
		//System.out.println(response);
		
		// LOAD DATA ON EMPLOYEE
        //String response = ws.callWSMethod("<Request Action=\"GetQueryCount\" > <HyperFindQuery HyperFindQueryName=\"Inactive or Term Employees\" QueryDateSpan=\"05/01/2013 - 08/01/2013\" VisibilityCode=\"Public\" /></Request>");        //String response = ws.callWSMethod("<Request Action=\"LoadDailyTotals\" > <Timesheet> <Period> <TimeFramePeriod TimeFrameName=\"Current Pay Period\"/> </Period> <Employee> <PersonIdentity PersonNumber=\"HIU0010035\" ><FilterQueryName><HyperFindQuery HyperFindQueryName=\"All Home\" /></FilterQueryName></PersonIdentity> </Employee> </Timesheet> </Request>");
        //String response = ws.callWSMethod("<Request Action=\"LoadPeriodTotals\" > <Timesheet> <Period> <TimeFramePeriod TimeFrameName=\"Current Pay Period\"/> </Period> <Employee> <PersonIdentity BadgeNumber=\"61\"/> </Employee> </Timesheet> </Request>");
        //String response = ws.callWSMethod("<Request Action=\"LoadDailyTotals\" > <Timesheet> <Period> <TimeFramePeriod TimeFrameName=\"Current Pay Period\"/> </Period> <Employee> <PersonIdentity BadgeNumber=\"5156\"/> </Employee> </Timesheet> </Request>");
		//String response = ws.callWSMethod("<Request Action=\"LoadPayCodeTotals\" Object=\"BulkTotals\" ><BulkTotals PayCodeNames=\"Regular,Overtime,Holiday\"><Employees><PersonIdentity BadgeNumber=\"61\"/></Employees><Period><TimeFramePeriod TimeFrameName=\"1\"/></Period></BulkTotals></Request>");
        //String response = ws.callWSMethod("<Request Action=\"LoadDailyTotals\" > <PayrollPrepTotals> <Period> <TimeFramePeriod TimeFrameName=\"Previous Pay Period\"/> </Period> <Employee> <PersonIdentity PersonNumber=\"HIU0000127\"/> </Employee> </PayrollPrepTotals> </Request>");
        // Check if the period is approved for a given employee
        //String response = ws.callWSMethod("<Request Action=\"PeriodIsApproved\" ><ApproveSignoff><Employee><PersonIdentity BadgeNumber=\"61\"/></Employee><Period> <TimeFramePeriod TimeFrameName=\"Current Pay Period\"/> </Period></ApproveSignoff></Request>");

		// ASSIGN PRIMARY LABOR ACCOUNT TO EMPLOYEE
		//String response = ws.callWSMethod("<Request Action=\"UpdateOnly\" Object=\"Personality\"> <Personality><Identity><PersonIdentity BadgeNumber=\"61\"/></Identity><JobAssignmentData><JobAssignment><PrimaryLaborAccounts><PrimaryLaborAccount EffectiveDate=\"1/02/2013\" ExpirationDate=\"3/26/2014\" LaborAccountName=\"110/0001/0000/56250/000064/BR1530/New Test Job\"/></PrimaryLaborAccounts></JobAssignment></JobAssignmentData></Personality> </Request>");
		
		// INSERT NEW JOB
		//String response = ws.callWSMethod("<Request Action=\"AddOnly\" Object=\"LaborLevelEntry\" LaborLevelEntryName=\"New Test Job\" LaborLevelDefinitionName=\"Job\"></Request>");
		//String response = ws.callWSMethod("<Request Action=\"AddOnly\" Object=\"LaborLevelEntry\"><LaborLevelEntry LaborLevelEntryName=\"1960565\" LaborLevelDefinitionName=\"Job\"/><LaborLevelEntry LaborLevelEntryName=\"1961565\" LaborLevelDefinitionName=\"Job\"/></Request>");
		//System.out.println(response);
		
		//String result = ws.generateXMLFromTable();
		//System.out.println(response);
        logger.info("Ending the process...");
	}
*/


    @Override
    public int serviceMain(String[] strings) throws ServiceException {
        String startingInfo = String.format("Starting the process (%s)...", VERSION);
        logger.info(startingInfo);
        Client ws = null;
        try {
            ws = Client.getClientObject();
        } catch (ParserConfigurationException e) {
            logger.error("ParseConfigurationException: " + e.getMessage());
        }


        while (!shutdown) {
            assert ws != null;
            try {
                ws.startSession();
            } catch (IOException e) {
                logger.error("IOException when starting session: " + e.getMessage());
            }
            try {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.DAY_OF_WEEK, cal.getActualMinimum(Calendar.DAY_OF_WEEK));
                cal.add(Calendar.WEEK_OF_YEAR, -1);
                Date firstDay = cal.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

                cal.set(Calendar.DAY_OF_WEEK, cal.getActualMaximum(Calendar.DAY_OF_WEEK));
                cal.add(Calendar.WEEK_OF_YEAR, 1);
                Date lastDay = cal.getTime();

                String dateSpan = String.format("%s - %s", sdf.format(firstDay), sdf.format(lastDay));

                ws.uploadJobs();
                ws.updateOracleData(dateSpan);
            } catch (Exception e) {
                logger.error("Error when executing process: " + e.getMessage());
            }

            try {
                Thread.sleep(600000);
            } catch (InterruptedException e) {
                logger.error("InterruptedException: "  + e.getMessage());
            }
        }
        logger.info("Ending the process...");
        return 0;
    }

}
