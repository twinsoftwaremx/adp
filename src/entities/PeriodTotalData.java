package entities;

/**
 * User: Rodolfo
 * Date: 6/5/13
 * Time: 4:41 PM
 */
public class PeriodTotalData {
    private String laborAccountName;
    private String amountInCurrency;
    private String payCodeName;
    private String amountInTime;
    private String lastTotalizationDateTime;
    private String recordStatus;
    private String recordDate;

    public String getLaborAccountName() {
        return laborAccountName;
    }

    public void setLaborAccountName(String laborAccountName) {
        this.laborAccountName = laborAccountName;
    }

    public String getAmountInCurrency() {
        return amountInCurrency;
    }

    public void setAmountInCurrency(String amountInCurrency) {
        this.amountInCurrency = amountInCurrency;
    }

    public String getPayCodeName() {
        return payCodeName;
    }

    public void setPayCodeName(String payCodeName) {
        this.payCodeName = payCodeName;
    }

    public String getAmountInTime() {
        return amountInTime;
    }

    public void setAmountInTime(String amountInTime) {
        this.amountInTime = amountInTime;
    }

    public String getLastTotalizationDateTime() {
        return lastTotalizationDateTime;
    }

    public void setLastTotalizationDateTime(String lastTotalizationDateTime) {
        this.lastTotalizationDateTime = lastTotalizationDateTime;
    }

    public String getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(String recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    @Override
    public String toString() {
        return "PeriodTotalData{" +
                "laborAccountName='" + laborAccountName + '\'' +
                ", amountInCurrency='" + amountInCurrency + '\'' +
                ", payCodeName='" + payCodeName + '\'' +
                ", amountInTime='" + amountInTime + '\'' +
                ", lastTotalizationDateTime='" + lastTotalizationDateTime + '\'' +
                ", recordStatus='" + recordStatus + '\'' +
                ", recordDate='" + recordDate + '\'' +
                '}';
    }
}
