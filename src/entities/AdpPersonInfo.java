package entities;

/**
 * Created by Rodolfo Baeza
 * Date: 6/26/13
 * Time: 12:39 PM
 * Summary:
 */
public class AdpPersonInfo {
    private String number;
    private String laborAccountName;
    private String payCodeName;
    private String amountInTime;
    private String recordDate;
    private String lastTotalizationDateTime;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getLaborAccountName() {
        return laborAccountName;
    }

    public void setLaborAccountName(String laborAccountName) {
        this.laborAccountName = laborAccountName;
    }

    public String getPayCodeName() {
        return payCodeName;
    }

    public void setPayCodeName(String payCodeName) {
        this.payCodeName = payCodeName;
    }

    public String getAmountInTime() {
        return amountInTime;
    }

    public void setAmountInTime(String amountInTime) {
        this.amountInTime = amountInTime;
    }

    public String getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(String recordDate) {
        this.recordDate = recordDate;
    }

    public String getLastTotalizationDateTime() {
        return lastTotalizationDateTime;
    }

    public void setLastTotalizationDateTime(String lastTotalizationDateTime) {
        this.lastTotalizationDateTime = lastTotalizationDateTime;
    }
}
