package entities;

/**
 * User: Rodolfo
 * Date: 6/5/13
 * Time: 4:34 PM
 */
public class Person {
    private String badgeNumber;
    private String payRuleName;
    private String baseWageHourly;
    private String effectiveDate;
    private String expirationDate;
    private String defLaborAccountName;
    private String hourlyRate;
    private String address;
    private String firstName;
    private String lastName;
    private String personNumber;
    private String fullTimePercentage;

    public String getBadgeNumber() {
        return badgeNumber;
    }

    public void setBadgeNumber(String badgeNumber) {
        this.badgeNumber = badgeNumber;
    }

    public String getPayRuleName() {
        return payRuleName;
    }

    public void setPayRuleName(String payRuleName) {
        this.payRuleName = payRuleName;
    }

    public String getBaseWageHourly() {
        return baseWageHourly;
    }

    public void setBaseWageHourly(String baseWageHourly) {
        this.baseWageHourly = baseWageHourly;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getDefLaborAccountName() {
        return defLaborAccountName;
    }

    public void setDefLaborAccountName(String defLaborAccountName) {
        this.defLaborAccountName = defLaborAccountName;
    }

    public String getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(String hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public String getFullTimePercentage() {
        return fullTimePercentage;
    }

    public void setFullTimePercentage(String fullTimePercentage) {
        this.fullTimePercentage = fullTimePercentage;
    }

    @Override
    public String toString() {
        return "Person{" +
                "badgeNumber='" + badgeNumber + '\'' +
                ", payRuleName='" + payRuleName + '\'' +
                ", baseWageHourly='" + baseWageHourly + '\'' +
                ", effectiveDate='" + effectiveDate + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", defLaborAccountName='" + defLaborAccountName + '\'' +
                ", hourlyRate='" + hourlyRate + '\'' +
                ", address='" + address + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", personNumber='" + personNumber + '\'' +
                ", fullTimePercentage='" + fullTimePercentage + '\'' +
                '}';
    }
}
